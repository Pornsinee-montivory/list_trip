<cfquery name="nametrip" datasource="triplist">
    SELECT * FROM plans
    WHERE plan_id = <cfqueryparam cfsqltype="cf_sql_integer" value="#URL.id#" />
</cfquery>

<cfinclude template="src/include/header.cfm">
<cfinclude template="src/middleware/authMiddleware.cfm">
<link rel="stylesheet" href="/assets/css/card.css">

<html>
    <body>
        <cfoutput>
            <div class="container mx-auto my-6">
                <cfinclude template="src/include/menu.cfm">
                <div class="flex">
                    <cfinclude template="src/include/nav.cfm">
                    <div class="border-card w-4/5 ml-20">
                        <div class="grid grid-cols-1 mb-6">
                            <div class="flex">
                                <a href="home.cfm"><img src="/assets/img/icon/arrow.svg" style="width:30px"></a>
                                <span class="text-3xl ml-5 ">#nametrip.plan_name#</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </cfoutput>
    </body>
</html>