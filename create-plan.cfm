<cfinclude template="src/include/header.cfm">
<cfinclude template="src/middleware/authMiddleware.cfm">
<link rel="stylesheet" href="/assets/css/card.css">
<html>
    <body>
        <cfoutput>
            <div class="container mx-auto my-6">
                <cfinclude template="src/include/menu.cfm">
                <div class="flex">
                    <cfinclude template="src/include/nav.cfm">
                    <div class="border-card w-4/5 ml-20 ">
                        <div class="grid grid-cols-1 mb-6">
                            <div class="flex">
                                <span class="text-3xl mr-4">Create your plan</span>
                            </div>
                        </div>
                        <form action="act-create.cfm" method="POST">
                            <div class="grid grid-cols-1 mb-6">
                                <input name="pname" type="text" placeholder="Add your name trip ">
                            </div>
                            <div class="grid grid-cols-1 mb-6">
                                <input name="pbudget" type="text" placeholder="Add budget">
                            </div>
                            <div class="grid grid-cols-1 mb-6">
                                <input name="start_date" type="text" placeholder="start_date">
                            </div>
                            <div class="grid grid-cols-1 mb-6">
                                <input name="end_date" type="text" placeholder="end_date">
                            </div>
                            <div class="grid grid-cols-1 mb-6 mr-10">
                                <ul> Color
                                    <li></li>
                                    <li class="color-items " id="blue"></li>
                                    <li class="color-items" id="yellow"></li>
                                    <li class="color-items" id="purple"></li>
                                    <li class="color-items" id="red"></li>
                                    <li class="color-items" id="green"></li>
                                    <li class="color-items" id="pink"></li>
                                </ul>
                            </div>
                            <button class="primary-button w-full pr-2">
                                create a plan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </cfoutput>
    </body>
</html>