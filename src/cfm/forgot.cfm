<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listrip</title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/tailwind.css">
    <link rel="stylesheet" href="/assets/css/card.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@500&family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d1d928628a.js" crossorigin="anonymous"></script>
        
</head>
<body class="container mx-auto my-6">
    <body class="container mx-auto my-6 ">
        <!-- Head -->
        <cfinclude template="../include/login-header.cfm">
        <div class="flex items-center justify-center mt-10 ">
            <div class=" bg-purple-50 rounded-xl p-10 md:p-20 shadow-lg">
                <form action="reset-password.cfm" method="POST">
                    <h1 class="text-3xl mb-10 text-center">Forgot Password?</h1>
                    <p class="mb-5 ">Confirm your email address <br>so you can reset your password.</p>
                    <div class="mb-5">
                       <input type="email" name="email" class=" px-3 py-2 w-60 rounded-md border border-purple-300 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Email">
                    </div>
                    
                    <div class="flex justify-center">
                        <button class= "mt-5 primary-button w-40">Submit</button>
                    </div>
                </form>
            </div>
        </div>
            
    </div>
    <!-- <div>
        <h1>Forgot Password?</h1>
        <p>Confirm your email address so you can reset 
            your password.</p>
        <div class="mt-5">
            <input type="email" placeholder="Enter your email">
        </div>
        <div>
            <button class="primary-button px-2 py-2 mt-2">
                create a plan
            </button>
        </div>
    </div> -->
</body>
</html>