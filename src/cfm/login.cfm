<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listrip</title>
    
    <!--<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href="/assets/css/tailwind.css">
    <link rel="stylesheet" href="/assets/css/styles.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@500&family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d1d928628a.js" crossorigin="anonymous"></script>
        
</head>
<body class="container mx-auto my-6 ">
    <cfinclude template="../include/login-header.cfm">
    <div class="flex items-center justify-center mt-10 ">
        <div class="bg-purple-50 rounded-xl p-10 md:p-20 shadow-lg">
            <form action="home.html" method="POST">
                <h1 class="text-3xl mb-10 text-center">Log in</h1>
                <div class="mb-5">
                <input type="email" name="email" class=" px-3 py-2 w-60 rounded-md border border-purple-300 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Email">
                </div>
                <div class="mb-5">
                    <input type="password" name="password" class="px-3 py-2 w-60 rounded-md border border-purple-300 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Password">
                    <a href="forgot.cfm" class="flex text-xs font-bold justify-end text-purple-800">forgot password?</a>
                </div>
                <div class="flex justify-center">
                    <button class= " mt-5 primary-button w-40">Login</button>
                </div>
            </form>
        </div>
    </div>
        
</body>
</html>