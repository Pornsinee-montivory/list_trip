<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listrip</title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/tailwind.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@500&family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d1d928628a.js" crossorigin="anonymous"></script> 
</head>
<body class="container mx-auto my-6 ">
    <cfinclude template = "../include/login-header.cfm">
    <div class="flex items-center justify-center mt-10">
        <div class="bg-purple-50 rounded-xl p-10 md:p-20 shadow-lg">
            <form action="signup-success.cfm" method="POST">
                <h1 class="text-3xl mb-10 text-center">Sign Up</h1>
                <div class="mb-5">
                    <input type="text" name="firstname" class="px-3 py-2 w-60 rounded-md border border-purple-300" placeholder="Firstname"><br>
                </div>
                <div class="mb-5">
                    <input type="text" name="lastname" class="px-3 py-2 w-60 rounded-md border border-purple-300" placeholder="Lastname"><br>
                </div>
                <div class="mb-5">
                    <input type="email" name="email" class=" px-3 py-2 w-60 rounded-md border border-purple-300" placeholder="Email"><br>
                </div>
                <div class="mb-5">
                    <input type="password" name="password" class=" px-3 py-2 w-60 rounded-md border border-purple-300" placeholder="Password"><br>
                </div>
                <div class="mb-5">
                    <input type="password" name="cfpassword" class=" px-3 py-2 w-60 rounded-md border border-purple-300" placeholder="Confirm Password"><br>
                </div>
                <div class="flex justify-center">
                    <button class= "mt-5 primary-button w-40">Sign up</button>
                </div>
            </form>
        </div>
    </div>
    
    
</body>    
</html>