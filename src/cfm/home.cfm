<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listrip</title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/tailwind.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@500&family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d1d928628a.js" crossorigin="anonymous"></script>
        
</head>
<body class="container px-6 md:px-8 lg:mx-auto my-6">
    <div class="grid grid-cols-2 gap-4 mb-4">
        <div>
            <a href="home.html"><h1 class="text-5xl header-text lg:ml-8">listrip</h1></a>
        </div>
        <div class="flex justify-end lg:mr-8">
            <div class="relative inline-block text-left">
                <div class="flex">
                    <div>
                        <a href="home.html"><img src="/assets/img/icon/home.svg" style="width: 30px;"></a>
                    </div>
                    <div>
                        <button type="button"
                        class="ml-5 inline-flex focus:outline-none "
                        id="options-menu" aria-haspopup="true" aria-expanded="true">
                        <img src="/assets/img/icon/user.svg" style="width: 30px;">
                        </button>
                    </div>
                </div>
                <!-- Dropdown Account Settings / Sign out -->
                <!-- <div
                    class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                    <div class="py-1" role="menu" aria-orientation="vertical"
                        aria-labelledby="options-menu">
                        <a href="##"
                            class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                            role="menuitem">Account settings</a>
                        <form method="POST" action="auth.cfm?service=logout">
                            <button type="submit"
                                class="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                                role="menuitem">
                                Sign out
                            </button>
                        </form>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <div class="flex">
        <cfinclude template="../include/nav.cfm">
        <div class="w-full lg:w-4/5 lg:ml-20">
            <div class="grid grid-cols-1 mb-6">
                <div class="flex">
                    <span class="text-3xl mr-4">Plans</span> 
                </div>
            </div>
            <div class="grid grid-cols-2 md:grid-cols-3 gap-4 md:gap-16 lg:gap-8"> 
                <div class="card-trip relative">
                    <div class="card-trip-content p-6 ">
                        <h1 class="text-xl pb-2 font-medium">Pattaya</h1>
                        <p>Budget : 12000 THB</p>
                        <p>Date : </p>
                    </div>
                    <div class="card-trip-action">abc</div>
                </div>
                <div class="card-trip relative">
                    <div class="card-trip-content p-6">
                        <h1 class="text-xl pb-2 font-medium">Pattaya</h1>
                        <p>Budget : 12000 THB</p>
                        <p>Date : </p>
                    </div>
                </div>
                <div class="card-trip relative">
                    <div class="card-trip-content p-6">
                        <h1 class="text-xl pb-2 font-medium">Pattaya</h1>
                        <p>Budget : 12000 THB</p>
                        <p>Date : </p>
                    </div>
                </div>
                <div class="card-trip relative">
                    <div class="card-trip-content p-6">
                        <h1 class="text-xl pb-2 font-medium">Pattaya</h1>
                        <p>Budget : 12000 THB</p>
                        <p>Date : </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>