<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listrip</title>
    <link rel="stylesheet" href="/assets/css/styles.css">
    <link rel="stylesheet" href="/assets/css/tailwind.css">
    <link rel="stylesheet" href="/assets/css/card.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@500&family=Nunito:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/d1d928628a.js" crossorigin="anonymous"></script>
        
</head>
<body class="container mx-auto my-6">
    <cfinclude template="../include/login-header.cfm">
    <div class="flex items-center justify-center mt-10 ">
        <div class=" bg-purple-50 rounded-xl p-10 md:p-20 shadow-lg">
            <h1 class="text-xl mb-10 text-center">Sign up success</h1>
            <div class="flex justify-center mb-5">
                <img src="/assets/img/icon/checked.svg" width="100px" alt="">
            </div>
            <div class="flex justify-center">
                <button class= "mt-5 primary-button w-40" onclick="window.location.href='login.cfm'">Login Now</button>
            </div>
        </div>
    </div>
</body>
</html>