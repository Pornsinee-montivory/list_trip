<cfquery datasource="triplist" name="plans">
    SELECT * FROM PLANS
    ORDER BY CREATED_AT
</cfquery>
<cfoutput>
    <div class="w-1/5 plan-left-section hidden lg:block">
        <div class="text-center mb-8 relative">
            
            <button class="primary-button w-full pr-2" onclick="location.href='create-plan.cfm'">
                <div class="absolute top-3.5 left-4 ">
                    <img src="/assets/img/icon/plus.svg" style=" width:15px;">
                </div>
                create a plan
            </button>
        </div>
        
        <div class="flex flex-col plan-content">
            <div class="flex mb-6">
                <span class="text-3xl font-medium mr-20">Plans</span>
                <img src="/assets/img/icon/down-arrow.svg" style="width: 15px;">
            </div>
                <cfloop query="plans">
                    <a href="list-show.cfm?id=#plan_id#">#plans.plan_name#</a>
                </cfloop>
        </div>
    </div>
</cfoutput>