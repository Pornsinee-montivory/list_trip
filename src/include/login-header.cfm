<cfif CGI.SCRIPT_NAME eq "/src/html/signup.cfm">
    <cfset buttonLogin = "mr-6 bg-purple-900 h-5/6 focus:outline-none px-4 py-2 rounded-xl text-white transform hover:scale-105 motion-reduce:transform-none">
    <cfset buttonLSignup = "mr-8 bg-white h-5/6 focus:outline-none px-3 py-2 rounded-xl text-purple-900 border border-purple-900 transform hover:scale-105 motion-reduce:transform-none">
<cfelse>
    <cfset buttonLogin = "mr-6 bg-white h-5/6 focus:outline-none px-4 py-2 rounded-xl text-purple-900 border border-purple-900 transform hover:scale-105 motion-reduce:transform-none">
    <cfset buttonLSignup = "mr-8 bg-purple-900 h-5/6 focus:outline-none px-3 py-2 rounded-xl text-white transform hover:scale-105 motion-reduce:transform-none">
</cfif>
<cfoutput>
    <div class="flex mb-4">
        <div class="w-1/3 sm:w-1/2">
            <h1 class="text-5xl header-text ml-8">listrip</h1>
        </div>
        <div class="w-2/3 sm:w-1/2 flex justify-end">
            <div class="relative inline-block text-left">
                <button class="#buttonLogin#" onclick="window.location.href='login.cfm'">
                    Log in
                </button>
            </div>
            <div class="relative inline-block text-left">
                <button class="#buttonLSignup#" onclick="window.location.href='signup.cfm'">
                    Sign up
                </button>
            </div>
        </div>
    </div>
</cfoutput>