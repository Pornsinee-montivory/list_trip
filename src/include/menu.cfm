<div class="grid grid-cols-2 gap-4 mb-4">
    <div>
        <a href="home.cfm"><h1 class="text-5xl header-text ml-8">listrip</h1></a>
    </div>
    <div class="flex justify-end mr-8">
        <div class="relative inline-block text-left">
            <div class="flex">
                <div>
                    <a href="home.cfm"><img src="/assets/img/icon/home.svg" style="width: 30px;"></a>
                </div>
                <div>
                    <button type="button"
                    class="ml-5 inline-flex focus:outline-none "
                    id="options-menu" aria-haspopup="true" aria-expanded="true">
                    <img src="assets/img/icon/user.svg" style="width: 30px;">
                    </button>
                </div>
            </div>
            <div
                class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                <div class="py-1" role="menu" aria-orientation="vertical"
                    aria-labelledby="options-menu">
                    <a href="##"
                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                        role="menuitem">Account settings</a>
                    <form method="POST" action="auth.cfm?service=logout">
                        <button type="submit"
                            class="block w-full text-left px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900 focus:outline-none focus:bg-gray-100 focus:text-gray-900"
                            role="menuitem">
                            Sign out
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

