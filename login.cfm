
<cfinclude template="src/include/header.cfm" />
<cfinclude template="src/middleware/authMiddleware.cfm">

<html>
    <body class="container mx-auto my-6 bg-yellow-100">
        <div class="grid grid-cols-2 gap-4 mb-4">
            <div>
                <h1 class="text-5xl header-text ml-8">listrip</h1>
            </div>
            <div class="flex justify-end mr-8">
                <div class="relative inline-block text-left">
                    <button class="mr-10  focus:outline-none" onclick="window.location.href='login.cfm'">
                        Log in
                    </button>
                </div>
                <div class="relative inline-block text-left">
                    <button class="mr-10  focus:outline-none" onclick="window.location.href='signup.cfm'">
                        Sign up
                    </button>
                </div>
            </div>
        <cfoutput>
            <form action="auth.cfm?service=login" method="POST">
                <div class="mb-5">
                    Email: <input type="email" name="email" class=" pl-3"><br>
                </div>
                <div class="mb-5">
                    Password: <input type="password" name="password" class=" pl-3"><br>
                </div>
                <cfif structKeyExists(url, 'error') and url.error eq 1>
                    <p class="text-red-600 text-sm">Email or Password incorrent!</p>
                <cfelseif structKeyExists(url, 'error') and url.error eq 2> 
                    <p class="text-red-600 text-sm">Your session is expire</p>
                </cfif>
                <input type="submit" value="Login" class= "mt-5 primary-button w-24 h-10">
                
            </form>
                
        </cfoutput>

        <!--- <cfoutput>#session.sessionID# #session.name#</cfoutput> --->
    </body>
</html>


