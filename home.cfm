<html>
    <!--- <cfdump var="#plans#"> --->
    <cfinclude template="src/middleware/authMiddleware.cfm"/>
    <cfinclude template="src/include/header.cfm"/>
    <body>
        <cfoutput>
            <div class="container mx-auto my-6">
                <cfinclude template="src/include/menu.cfm">
                <div class="flex">
                    <cfinclude template="src/include/nav.cfm">
                    <div class="w-4/5 ml-20">
                        <div class="grid grid-cols-1 mb-6">
                            <div class="flex">
                                <span class="text-3xl mr-4">Plans</span> 
                            </div>
                        </div>
                        <div class="grid grid-cols-3 gap-8"> 
                            <cfloop query="plans">
                                <div class="card-trip relative">
                                    <div class="card-trip-content p-6">
                                        <h1 class="text-xl pb-2 font-medium">#plans.plan_name#</h1>
                                        <p>Budget : #plans.budget# THB</p>
                                        <p>Date : </p>
                                    </div>
                                </div>
                            </cfloop>
                        </div>
                    </div>
                </div>
            </div>
        </cfoutput>
    </body>
</html>
